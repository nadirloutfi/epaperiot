
#include <Arduino.h>

#include <map>

using namespace std;

class TopicEnabler
{
private:
    /* data */
    std::map<String, bool> enabler; 
public:
    TopicEnabler(/* args */);
    ~TopicEnabler();
    void enable(String topic, bool enable);
    bool isEnabled(String topic);
};
