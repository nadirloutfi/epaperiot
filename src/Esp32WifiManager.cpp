#include "Esp32WifiManager.h"
#if defined(ESP8266)
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif

//needed for library
#include <DNSServer.h>
#if defined(ESP8266)
#include <ESP8266WebServer.h>
#else
#include <WebServer.h>
#endif

Esp32WifiManager::Esp32WifiManager(String user, String password)
{
    this->user = user;
    this->password = password;
}

void Esp32WifiManager::connect()
{
    if (!wifiManager.autoConnect("NADIRLOUTFI", "Othman0311"))
    {
        Serial.println("failed to connect, we should reset as see if it connects");
        delay(3000);
    }

    //if you get here you have connected to the WiFi
    Serial.println("connected...yeey :)");
    Serial.println(wifiManager.getSSID());

    Serial.println("local ip");
    Serial.println(WiFi.localIP());
}

String Esp32WifiManager::getSSID() {
    return wifiManager.getSSID();
}

Esp32WifiManager::~Esp32WifiManager()
{
}
