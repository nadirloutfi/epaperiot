#include "MQTTClient.h"

MQTTClient::MQTTClient(String host, int port, MQTTClient::TCallbackHandler callback)
{
    this->host = host;
    espClient = new WiFiClient();
    client = new PubSubClient(*espClient);
    client->setServer(this->host.c_str(), 1883);
    client->setCallback(callback);
}

MQTTClient::~MQTTClient()
{
    delete client;
    delete espClient;
}

void MQTTClient::connect(MQTTClient::TSuccessHandler fnSuccess, MQTTClient::TErrorHandler fnError)
{
    if (!client->connected())
    {
        Serial.println("Connecting to MQTT server...");
        // Create a random client ID
        String clientId = "ESP32Client-";
        clientId += String(random(0xffff), HEX);
        // Attempt to connect
        if (client->connect(clientId.c_str()))
        {
            Serial.println("Connected to MQTT server");
            fnSuccess();
        }
        else
        {
            Serial.println("Error connecting to MQTT server:");
            Serial.print(this->host);
            fnError(client);
        }
    }
    client->loop();
}

void MQTTClient::publish(String topic, String payload, bool retain)
{
    client->publish(topic.c_str(), payload.c_str(), retain);
}

void MQTTClient::subscribe(String topic)
{
    client->subscribe(topic.c_str());
}
