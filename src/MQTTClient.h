#include <WiFiClient.h>
#include <PubSubClient.h>

class MQTTClient
{
private:
    typedef std::function<void(void)> TSuccessHandler;
    typedef std::function<void(PubSubClient*)> TErrorHandler;
    typedef std::function<void(char *, byte *, unsigned int)> TCallbackHandler;
    /* data */
    PubSubClient *client;
    WiFiClient *espClient;
    String host;
public:
    MQTTClient(String host, int port, MQTTClient::TCallbackHandler callback);
    ~MQTTClient();
    void publish(String topic, String payload, bool retain);
    void subscribe(String topic);
    void connect(MQTTClient::TSuccessHandler success, MQTTClient::TErrorHandler error);
};
