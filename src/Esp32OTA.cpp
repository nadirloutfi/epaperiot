#include "Esp32OTA.h"
#include <WiFi.h>
#include <WiFiClient.h>
#include <ESPmDNS.h>
#include <Update.h>

Esp32OTA::Esp32OTA(/* args */)
{
    server = new WebServer(80);
}

void Esp32OTA::handleClient() {
    server->handleClient();
}
void Esp32OTA::begin(const char* host)
{
    /*use mdns for host name resolution*/
    if (!MDNS.begin(host))
    { //http://<host>.local
        Serial.println("Error setting up MDNS responder!");
        while (1)
        {
            delay(1000);
        }
    }
    Serial.println("mDNS responder started");
    /*return index page which is stored in serverIndex */
    server->on("/", HTTP_GET, [&]() {
        server->sendHeader("Connection", "close");
        server->send(200, "text/html", loginIndex);
    });
    server->on("/serverIndex", HTTP_GET, [&]() {
        server->sendHeader("Connection", "close");
        server->send(200, "text/html", serverIndex);
    });
    /*handling uploading firmware file */
    server->on("/update", HTTP_POST, [&]() {
        server->sendHeader("Connection", "close");
        server->send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
        ESP.restart(); }, [&]() {
            HTTPUpload& upload = server->upload();
            if (upload.status == UPLOAD_FILE_START) {
                Serial.printf("Update: %s\n", upload.filename.c_str());
                if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
                    Update.printError(Serial);
                }
            } else if (upload.status == UPLOAD_FILE_WRITE) {
                /* flashing firmware to ESP*/
                if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
                    Update.printError(Serial);
                }
            } else if (upload.status == UPLOAD_FILE_END) {
                if (Update.end(true)) { //true to set the size to the current progress
                    Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
                } else {
                    Update.printError(Serial);
                }
            } 
        });
    server->begin();
}

Esp32OTA::~Esp32OTA()
{
    delete server;
}
