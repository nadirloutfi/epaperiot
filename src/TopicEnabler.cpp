#include "TopicEnabler.h"

TopicEnabler::TopicEnabler(/* args */)
{
}

void TopicEnabler::enable(String topic, bool enable)
{
    enabler.insert(pair<String, bool>(topic, enable));
}

bool TopicEnabler::isEnabled(String topic)
{
    bool value = false;
    Serial.print(topic);
    if (enabler.find(topic) != enabler.end()) {
        value = enabler[topic];
    }
    Serial.print(":");
    Serial.println(value);
    return value;
}

TopicEnabler::~TopicEnabler()
{
}
