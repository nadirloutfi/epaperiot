
#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h> // https://github.com/bblanchon/ArduinoJson
#include "time.h"
#include <GxEPD.h>
#include <GxGDEW042T2/GxGDEW042T2.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>
#include <PubSubClient.h>
#include <Fonts/FreeSansBold18pt7b.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/FreeSerif9pt7b.h>

#include "Esp32OTA.h"
#include "MQTTClient.h"
#include "TopicEnabler.h"
#include "Esp32WifiManager.h"
#include "Constants.h"

#define BOX_SIZE_WIDTH 130
#define BOX_SIZE_HEIGHT 130
#define PADDING 5


GxIO_Class io(SPI, /*CS=5*/ SS, /*DC=*/17, /*RST=*/16); // arbitrary selection of 17, 16
GxEPD_Class display(io, /*RST=*/16, /*BUSY=*/4);        // arbitrary selection of (16), 4

//################ PROGRAM VARIABLES and OBJECTS ################
unsigned long lastConnectionTime = 0;          // Last time you connected to the server, in milliseconds
const unsigned long postingInterval = 120000L; // Delay between updates, in milliseconds
String time_str;                               // strings to hold time and received weather data;

typedef enum
{
  AUTO,
  BOTTOM
} LAYOUT_POSITION;

typedef struct
{
  char header[100];
  char body[150];
  char footer[100];
  char topic[100];
  LAYOUT_POSITION position;
  bool enabled;
} Iot;

Iot *sensors;
int iotCurrentIndex;

typedef struct
{
  uint16_t x;
  uint16_t y;
  uint16_t width;
  uint16_t height;
} Box;


Box start;
uint16_t MAXBOXES, EPAPER_WIDTH, EPAPER_HEIGHT;
Esp32WifiManager wifiManager(Constants::WM_USER, Constants::WM_USER);
Esp32OTA ota;
TopicEnabler topicEnbaler;


int StartWiFi(const char *ssid, const char *password);
void callback(char *topic, byte *payload, unsigned int length);
void Reconnect();
void displayDate();
void showPartialUpdate(Iot &iot, Box &start);
void DisplayText(int x, int y, String text);
void subscribeTopic();
void UpdateTime();
int getMaxNbBoxes(int width, int height);
void clear_screen();
void mqttConnect();

MQTTClient client(String(Constants::MQTT_SERVER), 1883, callback);

void setup()
{
  Serial.begin(115200);
  wifiManager.connect();
  ota.begin("esp32");

  display.init();
  display.fillScreen(GxEPD_WHITE);
  display.setRotation(2);
  display.update();
  EPAPER_WIDTH = display.width();
  EPAPER_HEIGHT = display.height();
  MAXBOXES = getMaxNbBoxes(EPAPER_WIDTH, EPAPER_HEIGHT) + 1;
  Serial.print("MAXBOXES:");
  Serial.println(MAXBOXES);
  start = {PADDING, 0, BOX_SIZE_WIDTH, BOX_SIZE_HEIGHT};
  sensors = (Iot *)malloc(MAXBOXES * sizeof(Iot));
  lastConnectionTime = millis();
}

void mqttConnect()
{
  client.connect([&]() {
    Serial.println("connected");
    char mac_addr[20];
    byte mac[6];
    WiFi.macAddress(mac);
    sprintf(mac_addr, "%02x:%02x:%02x:%02x:%02x:%02x",
            mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    Serial.print("MAC address = ");
    Serial.println(mac_addr);

    client.publish(String("/iot/configuration/SSID"), String(wifiManager.getSSID()), true);
    client.publish(String("/iot/configuration/ip"), String(WiFi.localIP()), true);
    client.publish(String("/iot/configuration/MAC"), String(mac_addr), true);

    subscribeTopic();

    delay(100); },
                 [&](PubSubClient *client) {
                   Serial.print("failed, rc=");
                   Serial.print(client->state());
                   Serial.println(" try again in 5 seconds");
                   // Wait 5 seconds before retrying
                   delay(5000); });
}

bool isDeepSleepEnabled()
{
  return topicEnbaler.isEnabled("/iot/screen/ds");
}

void loop()
{

  mqttConnect();

  ota.handleClient();

  delay(1);

  if (millis() - lastConnectionTime > postingInterval && iotCurrentIndex > 0)
  {
    clear_screen();
    lastConnectionTime = millis();

    Box box = start;
    Iot iot = sensors[0];
    bool widgetShown = false;
    for (int i = 0; i < iotCurrentIndex; i++)
    {
      iot = sensors[i];
      if (topicEnbaler.isEnabled(iot.topic))
      {
        if (iot.position == AUTO)
        {
          Serial.print(box.x);
          Serial.print(",");
          Serial.println(box.y);
          if (widgetShown)
          {
            if (box.x + start.width > EPAPER_HEIGHT)
            {
              box.x = start.x;
              box.y = start.y + start.height + PADDING;
            }
            else
            {
              box.x = box.x + start.width + PADDING;
            }
          }
        }
        showPartialUpdate(iot, box);
        widgetShown = true;
      }
    }

    display.update();
    if (isDeepSleepEnabled())
    {
      ESP.deepSleep(15 * 60 * 1000000); // Sleep for 15 minutes
    }
  }
}

int getMaxNbBoxes(int width, int height)
{
  Serial.println("Getting display size....");
  Serial.println("Width:" + String(width));
  Serial.println("Height:" + String(height));
  int maxBoxes = (width * height) / ((BOX_SIZE_WIDTH + PADDING) * (BOX_SIZE_HEIGHT + PADDING));
  Serial.println("Max boxes:" + String(maxBoxes));
  delay(100);
  return maxBoxes;
}

Iot *getIotFromTopic(Iot *iot)
{
  for (size_t i = 0; i < MAXBOXES; i++)
  {
    if (strcmp(sensors[i].topic, iot->topic) == 0 && strcmp(sensors[i].footer, iot->footer) == 0)
    {
      return &sensors[i];
    }
  }
  return NULL;
}

char *readData(byte *payload, unsigned int length)
{
  char *data = (char *)malloc(length + 1);

  memset(data, 0, length + 1);
  for (int i = 0; i < length; i++)
  {
    data[i] = (char)payload[i];
  }
  return data;
}

void callback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.println("]");

  const char *data = readData(payload, length);

  Serial.print("Payload arrived [");
  Serial.print(data);
  Serial.println("]");

  DynamicJsonDocument doc(512);
  deserializeJson(doc, data);

  if (strcmp(topic, "/iot/screen/enabler") == 0)
  {
    String topic_value = doc["topic"];
    bool enable = doc["enable"];
    topicEnbaler.enable(topic_value, enable);
  }
  else if (iotCurrentIndex < MAXBOXES)
  {
    if (String(data) != "{}")
    {
      if (doc.containsKey("position") == 1 &&
          doc.containsKey("header") == 1 &&
          doc.containsKey("body") == 1 &&
          doc.containsKey("footer") == 1)
      {
        Iot currentIot;

        strcpy(currentIot.topic, topic);
        strcpy(currentIot.header, doc["header"]);
        strcpy(currentIot.body, doc["body"]);
        strcpy(currentIot.footer, doc["footer"]);

        char position[2];
        strcpy(position, doc["position"]);

        Iot *iot = getIotFromTopic(&currentIot);
        if (iot == NULL)
        {
          iot = &sensors[iotCurrentIndex];
          iotCurrentIndex++;
        }

        strcpy(iot->topic, topic);
        strcpy(iot->header, currentIot.header);
        strcpy(iot->body, currentIot.body);
        strcpy(iot->footer, currentIot.footer);
        if (strcmp(position, "0") == 0)
        {
          iot->position = AUTO;
        }
        else
        {
          iot->position = BOTTOM;
        }

        delay(1000);
      }
    }
  }
  delete data;
}

void displayDate()
{
  display.setTextColor(GxEPD_BLACK);
  display.setFont(&FreeSans9pt7b);
  //UpdateTime();
  DisplayText(85, 292, "Hello");
}

void showPartialUpdate(Iot &iot, Box &start)
{
  const GFXfont *fValue = &FreeSansBold18pt7b;
  const GFXfont *fLabel = &FreeSans9pt7b;

  double rotation = display.getRotation();

  if (iot.position == AUTO)
  {
    uint16_t box_x = start.x;
    uint16_t box_y = start.y;
    uint16_t box_w = start.width;
    uint16_t box_h = start.height;
    uint16_t cursor_y = box_y + 16;

    display.fillRect(box_x, box_y, box_w, box_h, GxEPD_BLACK);

    display.setFont(fLabel);
    display.setTextColor(GxEPD_WHITE);

    // Header
    display.setCursor(box_x + 5, cursor_y + 5);
    display.print(iot.header);

    // Footer
    display.setCursor(box_x + 5, box_y + box_h - 10);
    display.print(iot.footer);

    // Body
    display.setFont(fValue);
    display.setCursor(box_x + 20, cursor_y + 50);
    display.print(iot.body);
  }
  else
  {
    display.setTextColor(GxEPD_BLACK);
    display.setFont(&FreeSerif9pt7b);
    DisplayText(5, 292, iot.body);
  }
  //display.updateWindow(box_x, box_y, box_w, box_h, true);

  display.setRotation(rotation);

  delay(100);
}

void DisplayLine(String text)
{
  Serial.println(text);
  display.println(text);
}

void DisplayText(int x, int y, String text)
{
  display.setCursor(x, y);
  display.println(text);
}

int StartWiFi(const char *ssid, const char *password)
{
  int connAttempts = 0;
  Serial.print(F("\r\nConnecting to: "));
  Serial.println(String(ssid));
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    if (connAttempts > 50)
      return -5;
    connAttempts++;
  }
  Serial.print(F("WiFi connected at: "));
  Serial.println(WiFi.localIP());
  return 1;
}

void subscribeTopic()
{
  client.subscribe(String("/iot/screen/#"));
}

void clear_screen()
{
  display.fillScreen(GxEPD_WHITE);
  display.update();
}

void SetupTime()
{
  configTime(0 * 3600, 3600, "0.uk.pool.ntp.org", "time-a-g.nist.gov");
  UpdateTime();
}

void UpdateTime()
{
  struct tm timeinfo;
  while (!getLocalTime(&timeinfo))
  {
    Serial.println("Failed to obtain time");
  }
  //See http://www.cplusplus.com/reference/ctime/strftime/
  Serial.println(&timeinfo, "%a %b %d %Y   %H:%M:%S"); // Displays: Saturday, June 24 2017 14:05:49
  time_str = asctime(&timeinfo);                       // Displays: Sat Jun 24 14:05:49 2017
  time_str = time_str.substring(0, 24);                // Displays: Sat Jun 24 14:05
}
