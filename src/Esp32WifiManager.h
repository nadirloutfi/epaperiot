#include <Arduino.h>
#include <WiFiManager.h>

class Esp32WifiManager
{
private:
    /* data */
    String user;
    String password;
    WiFiManager wifiManager;
public:
    Esp32WifiManager(String user, String password);
    ~Esp32WifiManager();
    void connect();
    String getSSID();
};

